module.exports = {
  env: {
    browser: true,
    es6: true
  },
  extends: ["airbnb", "airbnb/hooks", "plugin:prettier/recommended"],
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly"
  },
  parser: "babel-eslint",
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 2018,
    sourceType: "module",
    babelOptions: {
      configFile: ".babelrc"
    }
  },
  plugins: ["react"],
  rules: {
    "import/prefer-default-export": 0,
    "import/export": "off"
  }
};
