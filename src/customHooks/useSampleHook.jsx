import { SAMPLE_CONSTANT } from "../constants";

const useSampleHook = props => {
  const { id } = props;
  let sampleValue = 0;

  switch (parseInt(id, 10)) {
    case SAMPLE_CONSTANT.ONE:
      sampleValue = 1;
      break;
    default:
      sampleValue = "";
  }

  return {
    sampleValue
  };
};

export default useSampleHook;
